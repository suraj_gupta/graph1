<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'propel_bundle_propel.command.migration_status_command' shared service.

include_once $this->targetDirs[3].'\\vendor\\symfony\\console\\Command\\Command.php';
include_once $this->targetDirs[3].'\\vendor\\symfony\\dependency-injection\\ContainerAwareInterface.php';
include_once $this->targetDirs[3].'\\vendor\\symfony\\framework-bundle\\Command\\ContainerAwareCommand.php';
include_once $this->targetDirs[3].'\\vendor\\propel\\propel-bundle\\Command\\FormattingHelpers.php';
include_once $this->targetDirs[3].'\\vendor\\propel\\propel-bundle\\Command\\AbstractCommand.php';
include_once $this->targetDirs[3].'\\vendor\\propel\\propel-bundle\\Command\\WrappedCommand.php';
include_once $this->targetDirs[3].'\\vendor\\propel\\propel-bundle\\Command\\MigrationStatusCommand.php';

$this->privates['propel_bundle_propel.command.migration_status_command'] = $instance = new \Propel\Bundle\PropelBundle\Command\MigrationStatusCommand();

$instance->setName('propel:migration:status');

return $instance;
