<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'fos_ck_editor.form.type' shared service.

include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\ConfigManagerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\ConfigManager.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\PluginManagerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\PluginManager.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\StylesSetManagerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\StylesSetManager.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\TemplateManagerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\TemplateManager.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\ToolbarManagerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Model\\ToolbarManager.php';
include_once $this->targetDirs[3].'\\vendor\\symfony\\form\\FormTypeInterface.php';
include_once $this->targetDirs[3].'\\vendor\\symfony\\form\\AbstractType.php';
include_once $this->targetDirs[3].'\\vendor\\friendsofsymfony\\ckeditor-bundle\\src\\Form\\Type\\CKEditorType.php';

$a = new \FOS\CKEditorBundle\Model\ConfigManager();
$a->setConfigs(array('my_config' => array('toolbar' => array(0 => array(0 => 'Bold', 1 => 'Italic', 2 => '-', 3 => 'NumberedList', 4 => 'BulletedList', 5 => '-', 6 => 'Link', 7 => 'Unlink', 8 => '-', 9 => 'About')), 'uiColor' => '#9AB8F3')));
$a->setDefaultConfig('my_config');

return $this->privates['fos_ck_editor.form.type'] = new \FOS\CKEditorBundle\Form\Type\CKEditorType($a, new \FOS\CKEditorBundle\Model\PluginManager(), new \FOS\CKEditorBundle\Model\StylesSetManager(), new \FOS\CKEditorBundle\Model\TemplateManager(), new \FOS\CKEditorBundle\Model\ToolbarManager());
