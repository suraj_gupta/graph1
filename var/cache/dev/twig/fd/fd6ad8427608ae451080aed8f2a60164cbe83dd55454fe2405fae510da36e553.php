<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_22a8244de576106d7934285d1988bfe6548abe5b92652c8676ed77097fe2acaf extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "
 <div>
        <div class=\"login_wrapper\">
            <div class=\"animate form login_form\">
                ";
        // line 10
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 10, $this->source); })())) {
            // line 11
            echo "                    <div class=\"alert alert-info\">
                        <div>";
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 12, $this->source); })()), "messageKey", array()), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 12, $this->source); })()), "messageData", array()), "security"), "html", null, true);
            echo "</div>
                    </div>
                ";
        }
        // line 15
        echo "                <section class=\"login_content\">
                    <form class=\"login-form\" action=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 17, $this->source); })()), "html", null, true);
        echo "\"/>
                        <h1>Login</h1>
                        <div>
                            <input type=\"text\" class=\"form-control\" placeholder=\"Username\" id=\"username\" name=\"_username\"
                       value=\"";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 21, $this->source); })()), "html", null, true);
        echo "\" required=\"required\" autofocus>
                        </div>
                        <div>
                            <input type=\"password\" class=\"form-control\" placeholder=\"Password\" id=\"password\" name=\"_password\"
                       required=\"required\">
                        </div>
                        <div>
                            <input type=\"submit\" class=\"btn btn-primary btn-lg btn-block\" id=\"_submit\" name=\"_submit\"
                   value=\"Login\"/>
                            ";
        // line 31
        echo "                        </div>

                        <div class=\"clearfix\"></div>

                        <div class=\"separator\">
                            <div class=\"clearfix\"></div>
                            <br />

                            <div>
                                <h1>Admin Panel</h1>
                                <p>©2018 All Rights Reserved.</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    ";
        // line 98
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 98,  96 => 31,  84 => 21,  77 => 17,  73 => 16,  70 => 15,  64 => 12,  61 => 11,  59 => 10,  53 => 6,  44 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}

 <div>
        <div class=\"login_wrapper\">
            <div class=\"animate form login_form\">
                {% if error %}
                    <div class=\"alert alert-info\">
                        <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
                    </div>
                {% endif %}
                <section class=\"login_content\">
                    <form class=\"login-form\" action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
                        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\"/>
                        <h1>Login</h1>
                        <div>
                            <input type=\"text\" class=\"form-control\" placeholder=\"Username\" id=\"username\" name=\"_username\"
                       value=\"{{ last_username }}\" required=\"required\" autofocus>
                        </div>
                        <div>
                            <input type=\"password\" class=\"form-control\" placeholder=\"Password\" id=\"password\" name=\"_password\"
                       required=\"required\">
                        </div>
                        <div>
                            <input type=\"submit\" class=\"btn btn-primary btn-lg btn-block\" id=\"_submit\" name=\"_submit\"
                   value=\"Login\"/>
                            {#<a class=\"reset_pass\" href=\"#\">Lost your password?</a>#}
                        </div>

                        <div class=\"clearfix\"></div>

                        <div class=\"separator\">
                            <div class=\"clearfix\"></div>
                            <br />

                            <div>
                                <h1>Admin Panel</h1>
                                <p>©2018 All Rights Reserved.</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    {#<div class=\"row\" style=\"margin-top: 1em;\">
        <div class=\"col-md-4\"></div>
        <div class=\"col-md-4\">
            <h1 style=\"text-align: center\">
                <a href=\"#\" class=\"logo\" style=\"float: none; font-size: 35px; text-align: center\"><b><span class=\"lite\">Login</span></b></a>
            </h1>
            <hr/>


        </div>
        <div class=\"col-md-4\"></div>
    </div>

    <form class=\"login-form\" action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\" style=\"margin: 15px auto 0;\">
        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\"/>
        <div class=\"login-wrap\">
            <p class=\"login-img\"><i class=\"icon_lock_alt\"></i></p>
            <div class=\"input-group\">
                <span class=\"input-group-addon\"><i class=\"icon_profile\"></i></span>
                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" id=\"username\" name=\"_username\"
                       value=\"{{ last_username }}\" required=\"required\" autofocus>
            </div>
            <div class=\"input-group\">
                <span class=\"input-group-addon\"><i class=\"icon_key_alt\"></i></span>
                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" id=\"password\" name=\"_password\"
                       required=\"required\">
            </div>
            <label class=\"checkbox\">
                <input type=\"checkbox\" value=\"remember-me\"> Remember me
            </label>
            <input type=\"submit\" class=\"btn btn-primary btn-lg btn-block\" id=\"_submit\" name=\"_submit\"
                   value=\"Login\"/>
        </div>
    </form>

    <div class=\"row\" >
        <div class=\"col-md-4\"></div>
        <div class=\"col-md-4\">
            <div class=\"separator\">
                <div class=\"clearfix\"></div>
                <div style=\"text-align: center\">
                    #}{#<h1 > <img style=\"height:2em;\" src=\"img/logo.png\" alt=\"\"></h1>#}{#
                    <p>©2018 All Rights Reserved. Bhumlichok Tourism, Gorkha</p>
                </div>
            </div>
        </div>
        <div class=\"col-md-4\"></div>
    </div>#}

{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "E:\\GIT\\graph\\templates\\bundles\\FOSUserBundle\\Security\\login.html.twig");
    }
}
