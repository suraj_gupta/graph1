<?php

/* flash.html.twig */
class __TwigTemplate_f1a3e7d5f0ea8a0918f4dea59f00e15b2acf26b73f043c7ac61717f5133b286d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "flash.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "flash.html.twig"));

        // line 1
        if ( !array_key_exists("close", $context)) {
            $context["close"] = false;
        }
        // line 2
        echo "
";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 3, $this->source); })()), "session", array()), "flashbag", array()), "get", array(0 => "alert"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 4
            echo "    <div class=\"alert alert-warning\" style=\"margin-top: 4em\">
        ";
            // line 5
            if ((isset($context["close"]) || array_key_exists("close", $context) ? $context["close"] : (function () { throw new Twig_Error_Runtime('Variable "close" does not exist.', 5, $this->source); })())) {
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";
            }
            // line 6
            echo "        ";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "
";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 10, $this->source); })()), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 11
            echo "    <div class=\"alert alert-danger\" style=\"margin-top: 4em\">
        ";
            // line 12
            if ((isset($context["close"]) || array_key_exists("close", $context) ? $context["close"] : (function () { throw new Twig_Error_Runtime('Variable "close" does not exist.', 12, $this->source); })())) {
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";
            }
            // line 13
            echo "        ";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 17, $this->source); })()), "session", array()), "flashbag", array()), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 18
            echo "    <div class=\"alert alert-info\" style=\"margin-top: 4em\">
        ";
            // line 19
            if ((isset($context["close"]) || array_key_exists("close", $context) ? $context["close"] : (function () { throw new Twig_Error_Runtime('Variable "close" does not exist.', 19, $this->source); })())) {
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";
            }
            // line 20
            echo "        ";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 24, $this->source); })()), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 25
            echo "    <div class=\"alert alert-success\" style=\"margin-top: 4em\">
        ";
            // line 26
            if ((isset($context["close"]) || array_key_exists("close", $context) ? $context["close"] : (function () { throw new Twig_Error_Runtime('Variable "close" does not exist.', 26, $this->source); })())) {
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";
            }
            // line 27
            echo "        ";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "flash.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 27,  115 => 26,  112 => 25,  108 => 24,  105 => 23,  95 => 20,  91 => 19,  88 => 18,  84 => 17,  81 => 16,  71 => 13,  67 => 12,  64 => 11,  60 => 10,  57 => 9,  47 => 6,  43 => 5,  40 => 4,  36 => 3,  33 => 2,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if close is not defined %}{% set close = false %}{% endif %}

{% for flashMessage in app.session.flashbag.get('alert') %}
    <div class=\"alert alert-warning\" style=\"margin-top: 4em\">
        {% if close %}<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>{% endif %}
        {{ flashMessage }}
    </div>
{% endfor %}

{% for flashMessage in app.session.flashbag.get('error') %}
    <div class=\"alert alert-danger\" style=\"margin-top: 4em\">
        {% if close %}<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>{% endif %}
        {{ flashMessage }}
    </div>
{% endfor %}

{% for flashMessage in app.session.flashbag.get('info') %}
    <div class=\"alert alert-info\" style=\"margin-top: 4em\">
        {% if close %}<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>{% endif %}
        {{ flashMessage }}
    </div>
{% endfor %}

{% for flashMessage in app.session.flashbag.get('success') %}
    <div class=\"alert alert-success\" style=\"margin-top: 4em\">
        {% if close %}<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>{% endif %}
        {{ flashMessage }}
    </div>
{% endfor %}
", "flash.html.twig", "E:\\GIT\\graph\\templates\\flash.html.twig");
    }
}
