<?php

/* home/index.html.twig */
class __TwigTemplate_f79a06c34070a666024f6cfce20ff92e1cd0bc1212d1e01d4c4029e5cb0a3daf extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("app.html.twig", "home/index.html.twig", 1);
        $this->blocks = array(
            'head_style' => array($this, 'block_head_style'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
            'body' => array($this, 'block_body'),
            'foot_script' => array($this, 'block_foot_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "app.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_head_style($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_style"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_style"));

        // line 4
        echo "    ";
        $this->displayParentBlock("head_style", $context, $blocks);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        echo "Dashboard";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 10
        echo "    <div class=\"row\" style=\"margin-top: 15em\">
        




";
        // line 17
        echo "
";
        // line 27
        echo "





        <canvas id=\"country\" height=\"357\" width=\"716\" style=\"width: 573px; height: 286px;\"></canvas>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 37
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 40
    public function block_foot_script($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "foot_script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "foot_script"));

        // line 41
        echo "    ";
        $this->displayParentBlock("foot_script", $context, $blocks);
        echo "
    <script src=\"/js/chart.js\"></script>



    <script>
        var ctx = document.getElementById(\"country\");
        var mybarChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [";
        // line 51
        echo (isset($context["date"]) || array_key_exists("date", $context) ? $context["date"] : (function () { throw new Twig_Error_Runtime('Variable "date" does not exist.', 51, $this->source); })());
        echo "],
                datasets: [{
                    label: \"year\",
                    backgroundColor: \"rgba(38, 185, 154, 0.31)\",
                    borderColor: \"rgba(38, 185, 154, 0.7)\",
                    pointBorderColor: \"rgba(38, 185, 154, 0.7)\",
                    pointBackgroundColor: \"rgba(38, 185, 154, 0.7)\",
                    pointHoverBackgroundColor: \"#fff\",
                    pointHoverBorderColor: \"rgba(220,220,220,1)\",
                    pointBorderWidth: 1,
                    data: [";
        // line 61
        echo (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 61, $this->source); })());
        echo "]
                },
        ";
        // line 64
        echo "        ";
        // line 65
        echo "        ";
        // line 66
        echo "        ";
        // line 67
        echo "        ";
        // line 68
        echo "        ";
        // line 69
        echo "        ";
        // line 70
        echo "        ";
        // line 71
        echo "        ";
        // line 72
        echo "        ";
        // line 73
        echo "        ";
        // line 74
        echo "                ]
             },
        });
    </script>

";
        // line 88
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 88,  207 => 74,  205 => 73,  203 => 72,  201 => 71,  199 => 70,  197 => 69,  195 => 68,  193 => 67,  191 => 66,  189 => 65,  187 => 64,  182 => 61,  169 => 51,  155 => 41,  146 => 40,  135 => 37,  126 => 36,  108 => 27,  105 => 17,  97 => 10,  88 => 9,  70 => 7,  57 => 4,  48 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'app.html.twig' %}

{% block head_style %}
    {{ parent() }}
{% endblock %}

{% block page_title %}Dashboard{% endblock %}

{% block content %}
    <div class=\"row\" style=\"margin-top: 15em\">
        




{#        <input type=\"text\" name=\"daterange\" value=\"01/01/2018 - 01/15/2018\" />#}

{#        <script>#}
{#            \$(function() {#}
{#                \$('input[name=\"daterange\"]').daterangepicker({#}
{#                    opens: 'left'#}
{#                }, function(start, end, label) {#}
{#                    console.log(\"A new date selection was made: \" + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));#}
{#                });#}
{#            });#}
{#        </script>#}






        <canvas id=\"country\" height=\"357\" width=\"716\" style=\"width: 573px; height: 286px;\"></canvas>
    </div>
{% endblock %}
{%  block body %}

{% endblock %}

{% block foot_script %}
    {{ parent() }}
    <script src=\"/js/chart.js\"></script>



    <script>
        var ctx = document.getElementById(\"country\");
        var mybarChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [{{ date|raw }}],
                datasets: [{
                    label: \"year\",
                    backgroundColor: \"rgba(38, 185, 154, 0.31)\",
                    borderColor: \"rgba(38, 185, 154, 0.7)\",
                    pointBorderColor: \"rgba(38, 185, 154, 0.7)\",
                    pointBackgroundColor: \"rgba(38, 185, 154, 0.7)\",
                    pointHoverBackgroundColor: \"#fff\",
                    pointHoverBorderColor: \"rgba(220,220,220,1)\",
                    pointBorderWidth: 1,
                    data: [{{ count|raw }}]
                },
        {#            {#}
        {#                label: \"browser\",#}
        {#                backgroundColor: \"rgba(3, 88, 106, 0.3)\",#}
        {#                borderColor: \"rgba(3, 88, 106, 0.70)\",#}
        {#                pointBorderColor: \"rgba(3, 88, 106, 0.70)\",#}
        {#                pointBackgroundColor: \"rgba(3, 88, 106, 0.70)\",#}
        {#                pointHoverBackgroundColor: \"#fff\",#}
        {#                pointHoverBorderColor: \"rgba(151,187,205,1)\",#}
        {#                pointBorderWidth: 1,#}
        {#                data: [{{ browser|raw}}]#}
        {#}#}
                ]
             },
        });
    </script>

{#<script>#}
{#    \$('#picker').datetimepicker({#}
{#        timepicker:false,#}
{#        datepicker:true,#}
{#        format:'Y-m-d',#}
{#        value:'2019-8-1',#}
{#        weeks:true#}
{#    })#}
{#</script>#}

{% endblock %}", "home/index.html.twig", "E:\\GIT\\graph\\templates\\home\\index.html.twig");
    }
}
