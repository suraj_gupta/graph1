<?php

/* app.html.twig */
class __TwigTemplate_38b3a65158e5cbd28a602d9758bddc24e274904ed394808a9845a475eff9d955 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'head_style' => array($this, 'block_head_style'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
            'foot_script' => array($this, 'block_foot_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "app.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "app.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    ";
        // line 11
        echo "    <title>Admin Panel</title>
    ";
        // line 12
        $this->displayBlock('head_style', $context, $blocks);
        // line 30
        echo "
</head>


<body class=\"nav-md\">
<script type=\"text/javascript\">
    var admin = {}
</script>
<div class=\"container body\">
    <div class=\"main_container\">
        <div class=\"col-md-3 left_col menu_fixed\">
            <div class=\"left_col scroll-view\">
                <div class=\"navbar nav_title\" style=\"border: 0;\">
                    <a href=\"/\" class=\"site_title\"><h1>Admin</h1></a>
                </div>

                <div class=\"clearfix\"></div>
                <br/>

                <!-- sidebar menu -->
                <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
                    <div class=\"menu_section\">
                        <ul class=\"nav side-menu\">
                            <li>
                                <a href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\">
                                    <i class=\"fa fa-home\"></i>
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href=\"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("users");
        echo "\">
                                    <i class=\"fa fa-users\"></i>
                                    Users
                                </a>
                            </li>
                            <li>
                                <a href=\"#\">
                                    <i class=\"fa fa-users\"></i>
                                    Menu
                                    <span class=\"fa fa-chevron-down\"></span>
                                </a>
                                <ul class=\"nav child_menu\" style=\"display: none;\">
                                    <li><a href=\"#\">Sub Menu 1</a></li>
                                    <li><a href=\"#\">Sub Menu 2</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <!-- top navigation -->
        <div class=\"top_nav\">
            <div class=\"nav_menu\">
                <nav>
                    <div class=\"nav toggle\">
                        <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
                    </div>
                    <div class=\"nav toggle\" style=\"margin-top: -1em\">
                        <h3>";
        // line 91
        $this->displayBlock('page_title', $context, $blocks);
        echo "</h3>
                    </div>


                    <ul class=\"nav navbar-nav navbar-right\">

                        <li class=\"\">
                            <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
                               aria-expanded=\"false\">
                                WELCOME ";
        // line 101
        echo "                                <span class=\" fa fa-angle-down\"></span>
                            </a>

                            <ul class=\"dropdown-menu dropdown-usermenu pull-right\" style=\"z-index: 999999\">
                                <li class=\"eborder-top\">
                                    <a href=\"";
        // line 106
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("change_password");
        echo "\"><i class=\"icon_profile\"></i> My Profile</a>
                                </li>
                                <li><a href=\"/logout\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->


    </div>
    <div class=\"right_col\" role=\"main\" style=\"height: fit-content\">
        ";
        // line 120
        $this->loadTemplate("flash.html.twig", "app.html.twig", 120)->display($context);
        // line 121
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 123
        echo "    </div>
</div>




";
        // line 129
        $this->displayBlock('foot_script', $context, $blocks);
        // line 167
        echo "
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 12
    public function block_head_style($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_style"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_style"));

        // line 13
        echo "        <link rel=\"stylesheet\" href=\"/vendor/bootstrap/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"/vendor/font-awesome/css/font-awesome.min.css\">
        <link rel=\"stylesheet\" href=\"/vendor/nprogress/nprogress.css\">
        <link rel=\"stylesheet\" href=\"/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css\">
        <link rel=\"stylesheet\" href=\"/css/custom.min.css\">

        <link rel=\"icon\" type=\"image/x-icon\" href=\"/img/logo.png\"/>
        <style>
            div.dataTables_processing {
                background-color: #688a7e;
            }

            .dataTables_length {
                padding: 7px;
            }
        </style>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 91
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 121
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 122
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 129
    public function block_foot_script($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "foot_script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "foot_script"));

        // line 130
        echo "    <script src=\"/vendor/jquery/jquery.min.js\"></script>
    <script src=\"/vendor/bootstrap/js/bootstrap.min.js\"></script>
    <script src=\"/vendor/fastclick/fastclick.js\"></script>
    <script src=\"/vendor/nprogress/nprogress.js\"></script>
    <script src=\"/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js\"></script>
    <script src=\"/js/custom.min.js\"></script>


";
        // line 142
        echo "
";
        // line 150
        echo "






    <!-- footer content -->
    <footer>
        <div class=\"pull-right\">
            &copy; 2018, All rights reserved.
        </div>
        <div class=\"clearfix\"></div>
    </footer>
    <!-- /footer content -->

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "app.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  274 => 150,  271 => 142,  261 => 130,  252 => 129,  242 => 122,  233 => 121,  216 => 91,  190 => 13,  181 => 12,  169 => 167,  167 => 129,  159 => 123,  156 => 121,  154 => 120,  137 => 106,  130 => 101,  118 => 91,  84 => 60,  75 => 54,  49 => 30,  47 => 12,  44 => 11,  33 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    {#<link rel=\"icon\" href=\"/images/logo_text.ico\" type=\"image/x-icon\"/>#}
    <title>Admin Panel</title>
    {% block head_style %}
        <link rel=\"stylesheet\" href=\"/vendor/bootstrap/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"/vendor/font-awesome/css/font-awesome.min.css\">
        <link rel=\"stylesheet\" href=\"/vendor/nprogress/nprogress.css\">
        <link rel=\"stylesheet\" href=\"/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css\">
        <link rel=\"stylesheet\" href=\"/css/custom.min.css\">

        <link rel=\"icon\" type=\"image/x-icon\" href=\"/img/logo.png\"/>
        <style>
            div.dataTables_processing {
                background-color: #688a7e;
            }

            .dataTables_length {
                padding: 7px;
            }
        </style>
    {% endblock head_style %}

</head>


<body class=\"nav-md\">
<script type=\"text/javascript\">
    var admin = {}
</script>
<div class=\"container body\">
    <div class=\"main_container\">
        <div class=\"col-md-3 left_col menu_fixed\">
            <div class=\"left_col scroll-view\">
                <div class=\"navbar nav_title\" style=\"border: 0;\">
                    <a href=\"/\" class=\"site_title\"><h1>Admin</h1></a>
                </div>

                <div class=\"clearfix\"></div>
                <br/>

                <!-- sidebar menu -->
                <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
                    <div class=\"menu_section\">
                        <ul class=\"nav side-menu\">
                            <li>
                                <a href=\"{{ path('home') }}\">
                                    <i class=\"fa fa-home\"></i>
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href=\"{{ path('users') }}\">
                                    <i class=\"fa fa-users\"></i>
                                    Users
                                </a>
                            </li>
                            <li>
                                <a href=\"#\">
                                    <i class=\"fa fa-users\"></i>
                                    Menu
                                    <span class=\"fa fa-chevron-down\"></span>
                                </a>
                                <ul class=\"nav child_menu\" style=\"display: none;\">
                                    <li><a href=\"#\">Sub Menu 1</a></li>
                                    <li><a href=\"#\">Sub Menu 2</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <!-- top navigation -->
        <div class=\"top_nav\">
            <div class=\"nav_menu\">
                <nav>
                    <div class=\"nav toggle\">
                        <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
                    </div>
                    <div class=\"nav toggle\" style=\"margin-top: -1em\">
                        <h3>{% block page_title %}{% endblock %}</h3>
                    </div>


                    <ul class=\"nav navbar-nav navbar-right\">

                        <li class=\"\">
                            <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
                               aria-expanded=\"false\">
                                WELCOME {#{{ app.user.username|upper }}#}
                                <span class=\" fa fa-angle-down\"></span>
                            </a>

                            <ul class=\"dropdown-menu dropdown-usermenu pull-right\" style=\"z-index: 999999\">
                                <li class=\"eborder-top\">
                                    <a href=\"{{ path('change_password') }}\"><i class=\"icon_profile\"></i> My Profile</a>
                                </li>
                                <li><a href=\"/logout\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->


    </div>
    <div class=\"right_col\" role=\"main\" style=\"height: fit-content\">
        {% include \"flash.html.twig\" %}
        {% block content %}
        {% endblock %}
    </div>
</div>




{% block foot_script %}
    <script src=\"/vendor/jquery/jquery.min.js\"></script>
    <script src=\"/vendor/bootstrap/js/bootstrap.min.js\"></script>
    <script src=\"/vendor/fastclick/fastclick.js\"></script>
    <script src=\"/vendor/nprogress/nprogress.js\"></script>
    <script src=\"/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js\"></script>
    <script src=\"/js/custom.min.js\"></script>


{#    <script type=\"text/javascript\" src=\"https://cdn.jsdelivr.net/jquery/latest/jquery.min.js\"></script>#}
{#    <script type=\"text/javascript\" src=\"https://cdn.jsdelivr.net/momentjs/latest/moment.min.js\"></script>#}
{#    <script type=\"text/javascript\" src=\"https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js\"></script>#}
{#    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css\" />#}

{#<script src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>#}
{#<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"></script>#}
{#    <link rel=\"stylesheet\" href=\"jquery.datetimepicker.min.js\">#}
{#    <script src=\"https://stackpath.booystrapcdn.com/bootstarp/4.3.1/js/bootstarp.min.js\"></script>#}
{#    <link href=\"https://stackpath.booystrapcdn.com/bootstarp/4.3.1/css/bootstarp.min.css\"#}
{#    rel=\"stylesheet\">#}
{#    <script src=\"jquery.datetimepicker.full.min.js\"></script>#}







    <!-- footer content -->
    <footer>
        <div class=\"pull-right\">
            &copy; 2018, All rights reserved.
        </div>
        <div class=\"clearfix\"></div>
    </footer>
    <!-- /footer content -->

{% endblock %}

</body>
</html>", "app.html.twig", "E:\\GIT\\graph\\templates\\app.html.twig");
    }
}
