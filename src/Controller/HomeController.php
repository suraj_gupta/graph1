<?php

namespace App\Controller;

use App\Form\Type\ProfileType;
use App\Model\DataBrowserQuery;
use App\Model\DataCountry;
use App\Model\DataCountryQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     * @return Response
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function indexAction()
    {

//        $ac = DataCountryQuery::create()->limit(5)->find();
//
//       foreach($ac as $abc)
//       {
//        $a[]=$abc->getCountry();
//        $ab[]=$abc->getCount();
//       }
//       $a=implode('","',$a);
////      print_r($a);
//       $a=sprintf('%s%s%s','"',$a,'"');
//       $ab=implode(',',$ab);
////       print_r($a);
////       print_r($ab);


       $browcount=DataBrowserQuery::create()->limit(5)->find();

        foreach($browcount as $abc)
        {
            $browser[]=$abc->getBrowser();
            $count[]=$abc->getCount();
            $date[]=$abc->getDate()->format('Y-m-d');
        }
        $browser=implode('","',$browser);
//       print_r($browser);
      $browser=sprintf('%s%s%s','"',$browser,'"');
       //print_r($browser);
      $count=implode(',',$count);
        //print_r($date);
        $date=implode('","',$date);
        $date=sprintf('%s%s%s','"',$date,'"');
        //$date=implode(',',$date);
        print_r($date);

        // print_r($browser);
      //print_r($count);



       return $this->render('home/index.html.twig', ['browser'=>$browser,'count'=>$count,'date'=>$date]);
    }

    /**
     * @Route("/change-password", name="change_password")
     **/
    public function passwordAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(
            ProfileType::class,
            $user
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $this->get('session')->getFlashBag()->add(
                'success',
                'Password was successfully updated.'
            );

            return $this->redirect($this->generateUrl('change_password'));
        }

        return $this->render('home/password.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
